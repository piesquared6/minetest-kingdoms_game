# worldedge

A mod that prevents digging/placing/walking outside a world edge.

## API

 - `worldedge.edge` *(read-only)*: The world edge. Changing this does nothing
    for performance reasons.
 - `worldedge.set_worldedge(edge)`: Changes `worldedge.edge`.
 - `worldedge.outside_edge(pos)`: Returns `true` if `pos` is outside the world
    edge.
 - `worldedge.reprimand_player(player, [msg])`: Tells a player that they can't
    interact outside the world edge and then ensures that they aren't outside
    the world edge. The message can be overridden with the optional `msg`
    parameter and set to an empty string to be disabled.
