--
-- Allow "/tc" and "/ac" to be sent
--

local channel

sscsm.register_chatcommand('tc', function(param)
    if param == '' then
        if channel == 'tc' then
            channel = nil
            return true, 'Messages will now be sent to normal chat by default.'
        else
            channel = 'tc'
            return true, 'Messages will now be sent to /tc by default. ' ..
                'Run /say to disable this.'
        end
    end

    return minetest.run_server_chatcommand('tc', param)
end)

sscsm.register_chatcommand('ac', function(param)
    if param == '' then
        if channel == 'ac' then
            channel = nil
            return true, 'Messages will now be sent to normal chat by default.'
        else
            channel = 'ac'
            return true, 'Messages will now be sent to /ac by default. ' ..
                'Run /say to disable this.'
        end
    end

    return minetest.run_server_chatcommand('ac', param)
end)

sscsm.register_chatcommand('say', function(param)
    if param == '' then
        if channel then
            channel = nil
            return true, 'Messages will now be sent to normal chat by default.'
        else
            return false, 'Messages are already being sent to normal chat.'
        end
    elseif param:sub(1, 1) == '/' then
        -- Add U+200B
        param = '\226\128\139' .. param
    end

    minetest.send_chat_message(param)
end)

minetest.register_on_sending_chat_message(function(message)
    local char = message:sub(1, 1)
    if channel and char ~= '/' and char ~= '.' then
        minetest.run_server_chatcommand(channel, message)
        return true
    end
end)
