# Persistent Kingdoms gamemode [kingdoms_game]

This is the game used for the [Minetest](https://github.com/minetest/minetest)
server "Persistent Kingdoms". Some of the "default" mods are modified, and
others are submodules.

### Submodules are fun.

When you clone this repo, make sure you are using `--recursive`:

```
git clone --recursive https://gitlab.com/luk3yx/minetest-kingdoms_game.git
```

To fetch the latest updates, you need to run two commands:

```
git pull
git submodule update --init --recursive
```
